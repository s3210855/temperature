package nl.utwente.di.temperature;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TemperatureConversionsTest {
    @Test
    public void TestCelsiusToFahrenheit(){
        assertEquals(50, TemperatureConversions.CelsiusToFahrenheit(10), 0.0);
    }
}
