package nl.utwente.di.temperature;

public class TemperatureConversions {
    public static double CelsiusToFahrenheit(double num) {
        return num * 1.8 + 32;
    }
}
